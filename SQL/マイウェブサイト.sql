SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS fav_t;
DROP TABLE IF EXISTS m_difficult;
DROP TABLE IF EXISTS m_duo;
DROP TABLE IF EXISTS m_easy;
DROP TABLE IF EXISTS m_long;
DROP TABLE IF EXISTS m_middle;
DROP TABLE IF EXISTS m_normal;
DROP TABLE IF EXISTS m_short;
DROP TABLE IF EXISTS m_solo;
DROP TABLE IF EXISTS m_trio_quartet;
DROP TABLE IF EXISTS bgt;
DROP TABLE IF EXISTS user_t;




/* Create Tables */

CREATE TABLE bgt
(
	bg_id int NOT NULL AUTO_INCREMENT,
	name varchar(256) NOT NULL,
	detail varchar(256) NOT NULL,
	png varchar(256) NOT NULL,
	player int NOT NULL,
	time int NOT NULL,
	difficult varchar(256) NOT NULL,
	PRIMARY KEY (bg_id),
	UNIQUE (bg_id),
	UNIQUE (name),
	UNIQUE (detail),
	UNIQUE (png)
);


CREATE TABLE fav_t
(
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	bg_id int NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (id)
);


CREATE TABLE m_difficult
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_duo
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_easy
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_long
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_middle
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_normal
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_short
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_solo
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE m_trio_quartet
(
	id int NOT NULL AUTO_INCREMENT,
	bg_id int NOT NULL,
	name varchar(256) NOT NULL,
	detail varchar(256),
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (bg_id)
);


CREATE TABLE user_t
(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(256) NOT NULL,
	login_id varchar(256) NOT NULL,
	password varchar(256) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (login_id)
);



/* Create Foreign Keys */

ALTER TABLE fav_t
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_difficult
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_duo
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_easy
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_long
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_middle
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_normal
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_short
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_solo
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_trio_quartet
	ADD FOREIGN KEY (bg_id)
	REFERENCES bgt (bg_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE fav_t
	ADD FOREIGN KEY (user_id)
	REFERENCES user_t (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



