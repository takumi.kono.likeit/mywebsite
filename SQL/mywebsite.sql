create database mywebsite default character set utf8;

use mywebsite;

CREATE TABLE bgt IF EXISTS bgt;
CREATE TABLE fav_t IF EXISTS fav_t;
CREATE TABLE user_t IF EXISTS user_t;

CREATE TABLE `bgt` (
  `bg_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `heading` varchar(256) NOT NULL,
  `detail` varchar(256) NOT NULL,
  `png` varchar(256) NOT NULL,
  `minplayer` int NOT NULL,
  `maxplayer` int NOT NULL,
  `time` int NOT NULL,
  `difficult` varchar(256) NOT NULL,
  PRIMARY KEY (`bg_id`),
  UNIQUE KEY `bg_id` (`bg_id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `detail` (`detail`),
  UNIQUE KEY `png` (`png`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

CREATE TABLE fav_t
(
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	bg_id int NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (id)
);

CREATE TABLE user_t
(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(256) NOT NULL,
	login_id varchar(256) NOT NULL,
	password varchar(256) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (id),
	UNIQUE (login_id)
);

INSERT INTO mywebsite.bgt(name,heading,detail,png,minplayer,maxplayer,`time`,difficult) VALUES 
    ('ブルームサービス','君は勇敢かそれとも臆病か？<br>相手の行動を読み自分の行動を選択するバッティングゲーム','相手が何をしたいかを考え１０枚のカードから４枚を選択して、ゲームボード上にある塔に薬を届けよう！<br>臆病な行動を選べば効果は小さいが安全に行動を行うことが出来る。勇敢な行動を選べば大きい効果を望めるが、後の人が同じカードを持っていたら自分は何もできない。<br>これは、そういった駆け引きを行いながら勝利店を獲得するゲームだ。','1.png',2,5,60,'普通')
  , ('チケット・トゥ・ライド','カードを集めて線路を敷こう！','同じ色のカードを集めてボード上に線路を敷設していこう。<br>手元にある目的地カードに書かれた２つの地点を自分の線路で繋ぐと高得点だ！<br>選べる行動が３つしかないので、初心者にもおすすめのゲーム。','2.png',2,4,45,'簡単')
  , ('クラスク','電気を使わないエアホッケー','台の下で磁石を動かして、自分のスティックを操作しよう！<br>相手のゴールにボールを入れることが出来たら１点獲得だ！<br>ただしフィールドにある白い石には気を付けて、２つくっついちゃうと負けだよ。','11.png',2,2,20,'簡単')
  , ('宝石の煌き','宝石を集めて勝利点を稼ごう','最初は場にある宝石を集めてカードを購入しよう！<br>購入後はカードに描かれている宝石も購入するときに使えるぞ！<br>宝石をとる/カードを買う/カードを予約するの３つしか行動がないので、ルールが分かりやすい！','12.png',2,4,45,'普通')
  , ('エバーデール','様々な動物や建物が描かれたカード集めて点数を稼ごう！','手元にある自分の駒をゲームボード状のマスに置いて行動を行うゲームです。<br>ラウンドが進むごとに使える駒やカードが増えて出来ることが増えていきます！<br>場に出せるカードの枚数には上限があるので、よく考えて行動しよう！','10.png',1,4,90,'難しい');
