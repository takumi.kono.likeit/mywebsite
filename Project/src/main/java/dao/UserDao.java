package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.NameHeadingBeans;
import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
        try {
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user_t WHERE login_id = ? and password = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
	
	public static List<String> findId() {
		Connection conn = null;
		List<String> IdList = new ArrayList<String>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT login_id FROM user";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String id = rs.getString("login_id");
				IdList.add(id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return IdList;
	}

	public static void createUser(String id, String pw, String name) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "insert into user_t values(null, ? , ? , ?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, id);
			pStmt.setString(3, pw);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static List<NameHeadingBeans> findPlayer(String player) {
		Connection conn = null;
		List<NameHeadingBeans> list = new ArrayList<NameHeadingBeans>();
		try {
			conn = DBManager.getConnection();

			String sql = "select bg_id,name,heading from bgt where minplayer<=? && maxplayer>=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, player);
			pStmt.setString(2, player);
			ResultSet rs = pStmt.executeQuery();
			
			while (rs.next()) {
				int bg_id = rs.getInt("bg_id");
				String name = rs.getString("name");
				String heading = rs.getString("heading");
				NameHeadingBeans nhb = new NameHeadingBeans(bg_id,name,heading);

				list.add(nhb);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}
