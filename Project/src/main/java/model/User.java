package model;

import java.io.Serializable;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private String password;
	
	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	public User(String loginId, String name, int id) {
		this.loginId = loginId;
		this.name = name;
		this.id = id;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	}
	
