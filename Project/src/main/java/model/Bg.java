package model;

import java.io.Serializable;

public class Bg implements Serializable {
	private int id;
	private String name;
	private String heading;
	private String datail;
	private String png;
	private int minplayer;
	private int maxplayer;
	private int time;
	private String difficult;
	
	public Bg(String name,String heading) {
		this.name = name;
		this.heading = heading;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public String getDatail() {
		return datail;
	}
	public void setDatail(String datail) {
		this.datail = datail;
	}
	public String getPng() {
		return png;
	}
	public void setPng(String png) {
		this.png = png;
	}
	public int getMinplayer() {
		return minplayer;
	}
	public void setMinplayer(int minplayer) {
		this.minplayer = minplayer;
	}
	public int getMaxplayer() {
		return maxplayer;
	}
	public void setMaxplayer(int maxplayer) {
		this.maxplayer = maxplayer;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getDifficult() {
		return difficult;
	}
	public void setDifficult(String difficult) {
		this.difficult = difficult;
	}
}