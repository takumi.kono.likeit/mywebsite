package beans;

import java.io.Serializable;

public class NameHeadingBeans implements Serializable {
	private int bg_id;
	private String name;
	private String heading;
	
	public NameHeadingBeans(int bg_id,String name,String heading) {
		this.bg_id = bg_id;
		this.name = name;
		this.heading = heading;
	}	
	public int getBg_id() {
		return bg_id;
	}
	public void setBg_id(int bg_id) {
		this.bg_id = bg_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}

}
