<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utd-8">
    <title>TOPページ</title>
    <jsp:include page="/baselayout/head.html" />
</head>

<body>
 	<jsp:include page="/baselayout/header.jsp" />
<article>
	<jsp:include page="/baselayout/sidemenu.jsp" />
     
    <div class="content">
    	<br>
        <h1>BordGameSerchへようこそ！</h1><br>
        <h3>
            このサイトは、ボードゲームを始めたいけど、どう始めたらいいかわからない。<br>
            ゲームの種類が多すぎて何を遊べばいいかわからないといった初心者の方向けに、<br>
            様々なボードゲームの紹介を行うために作成しました！
        </h3>
     </div>
    
  </article>
<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>