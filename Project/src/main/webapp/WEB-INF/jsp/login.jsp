<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utd-8">
    <title>ログイン画面</title>
    <jsp:include page="/baselayout/head.html" />
</head>

<body>
 	<jsp:include page="/baselayout/header.jsp" />
<article>
	<jsp:include page="/baselayout/sidemenu.jsp" />
	<div class=content>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	
 <br><h1>ログイン画面</h1>

<br><br><br>

<form action=LoginServlet method="post">
    <div class="row" style="text-align:center;">
        <div class="col-sm-12">
					<h3>ログインID　　　
            		<input type="text"  name = "id" value = "${id}" style="width:200px;" placeholder="ID" required autofocus>
           			</h3>
    </div>
    </div>
    <div class="row" style="text-align:center;">
        <div class="col-sm-12">
        <h3>パスワード　　　
        <input type="password"  name="pw" style="width:200px;"placeholder="パスワード" required>
        </h3>
        </div>
        </div>

<br>

<div style="margin: 0 auto;width: 10%;">
<input style="text-align:center;" type="submit" value="  ログイン  ">
</div>

</form>
</div>
</article>
<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>