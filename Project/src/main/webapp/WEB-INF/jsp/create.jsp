<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utd-8">
    <title>新規登録</title>
   <jsp:include page="/baselayout/head.html" />
</head>

<body>
 	<jsp:include page="/baselayout/header.jsp" />
<article>
	<jsp:include page="/baselayout/sidemenu.jsp" />
	 <div class=content>
	<form action="CreateServlet" method="post">
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <br><h1>ユーザ新規登録</h1><br>
        <div class="row">
            <div class="col-sm-6">
            
            <h3>
                ログインID　
            </h3>
            </div>
            <div class="col-sm-6">
        	 <input type="text" name="id" value="${id}" 	style="width: 300px;" autofocus required>
            </div>
        </div><br>
         <div class="row">
            <div class="col-sm-6">
            <h3>
                パスワード
            </h3>
        </div>
            <div class="col-sm-6">
				<input type="text"  name = "pw" style="width:300px;"  required>
            </div>
        </div><br>
         <div class="row">
            <div class="col-sm-6">
            <h3>
                パスワード（確認）
            </h3>
        </div>
            <div class="col-sm-6">
				<input type="text"  name = "check" style="width:300px;" required >
            </div>
        </div><br>
             <div class="row">
            <div class="col-sm-6">
            <h3>
                ユーザ名
            </h3>
        </div>
            <div class="col-sm-6">
					 <input type="text" name="name" value="${name}" style="width: 300px;" required>
            </div>
            </div><br>
         <div style="text-align: center">
            <input type="submit" value="  　　登録  　　">
            </div><br>
        </form>
        </div>

</article>
<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>