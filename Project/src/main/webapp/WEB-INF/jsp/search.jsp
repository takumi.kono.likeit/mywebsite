<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utd-8">
    <title>検索</title>
    <jsp:include page="/baselayout/head.html" />
</head>

<body>
  <jsp:include page="/baselayout/header.jsp" />

<article>
    <jsp:include page="/baselayout/sidemenu.jsp" />
    <div class="content">
      <h2>ボードゲーム検索</h2><br>

    <form action="serchservlet" method="post">
      <div class="input-field col s12">
        <select name="pepole">
          <option value="" disabled selected>プレイ人数</option>
          <option value="1">１人</option>
          <option value="2">２人</option>
          <option value="3">３人</option>
        </select>
        <label>プレイ人数</label>
      </div>
      <div class="input-field col s12">
        <select>
          <option value="" disabled selected>プレイ時間</option>
          <option value="short">３０分以下</option>
          <option value="middle">３０分～６０分</option>
          <option value="long">６０分以上</option>
        </select>
        <label>プレイ時間</label>  
        </div> 
        <div class="input-field col s12">
          <select>
            <option value="" disabled selected>難易度</option>
            <option value="easy">簡単</option>
            <option value="normal">普通</option>
            <option value="hard">難しい</option>
          </select>
        <label>難易度</label>
        </div>
        <input type="submit" value="  　　検索  　　">
     </form>
    </div>
  </article>
  <jsp:include page="/baselayout/footer.jsp" />
</body>

</html>