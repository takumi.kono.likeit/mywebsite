<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utd-8">
    <title></title>
    <jsp:include page="/baselayout/head.html" />
</head>

<body>
 	<jsp:include page="/baselayout/header.jsp" />
<article>
	<jsp:include page="/baselayout/sidemenu.jsp" />
     
    <div class="content">
    	<br>
        <h1>検索結果</h1><br>
        <table class="bordered">
			<thead>
				<tr>
					<th class="center">名前</th>
					<th class="center" style="width: 75%">説明</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${nhb}">
					<tr>
						<td class="center"><a href=InfoServlet?=${user.bg_id}>${user.name}</a></td>
						<td class="center">${user.heading}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
     </div>
    
  </article>
<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>