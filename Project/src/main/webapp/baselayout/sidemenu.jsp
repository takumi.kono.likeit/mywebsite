<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="side">
        <ul class="collapsible expandable">
            <li>
                <div class="collapsible-header"><i class="material-icons">casino</i>ボードゲーム</div>
                <div class="collapsible-body">                        
                    <details>
                        <summary>プレイ人数</summary>
                       	　　<li><a href="PlayerServlet?id=1">・１人</a></li>
                        　　<li><a href="PlayerServlet?id=2">・２人</a></li>
                        　　<li><a href="PlayerServlet?id=3">・３人</a></li>
                    </details>
                    <br>
                    <details>
                        <summary>プレイ時間</summary>
                        　　<li>・３０分以下</li>
                        　　<li>・３０分～６０分</li>
                        　　<li>・６０分以上</li>
                    </details>   
                    <br>
                    <details>
                        <summary>難易度</summary>
                        　　<li>・簡単</li>
                        　　<li>・普通</li>
                        　　<li>・難しい</li>
                    </details>        
                    <br>
                    <details>
                        <summary>メカニクス</summary>
                        　　<li>・ワーカープレイスメント</li>
                        　　<li>・拡大再生産</li>
                        　　<li>・トリックテイキング</li>
                    </details>                      
                </div>
            </li>
            <li>
              <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
              <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
            </li>
            <li>
              <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
              <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
            </li>
        </ul>
  </div>