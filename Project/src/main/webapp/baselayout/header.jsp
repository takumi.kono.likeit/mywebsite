<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<nav>
    <div class="nav-wrapper blue lighten-2">
      <a href="ToppageServlet" class="brand-logo"><i class="material-icons">casino</i>BGS</a>
      <ul class="right hide-on-med-and-down">
      		<c:choose>
					<c:when test="${log != null}">
						 <li><a href="UserData"><i class="material-icons">account_circle</i></a></li>
					</c:when>
					<c:otherwise>
						 <li><a href="CreateServlet"><i class="material-icons">add</i></a></li>
					</c:otherwise>
			</c:choose>
	        <li><a href="SearchServlet"><i class="material-icons">search</i></a></li>
	        <c:choose>
					<c:when test="${log != null}">
						 <li><a href="LogoutServlet"><i class="material-icons">exit_to_app</i></a></li>
					</c:when>
					<c:otherwise>
						 <li><a href="LoginServlet"><i class="material-icons">vpn_key</i></a></li>
					</c:otherwise>
			</c:choose>
      </ul>
    </div>
  </nav>